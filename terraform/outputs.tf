output "network_id" {
  value = docker_network.application.id
}

output "network_name" {
  value = docker_network.application.name
}
